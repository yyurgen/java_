package com.gwain.authweb.servlets;

import com.gwain.authweb.entities.ChatUser;
import com.gwain.authweb.entities.ChatUsers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author 
 */
public class RegisterUserServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        final String user = req.getParameter("user");
        final ChatUsers users = (ChatUsers) getServletContext().getAttribute("users"); 
        ChatUser newUser = new ChatUser();
        newUser.setUser(user);
        String message = null;
        if (users.getUsers().containsKey(newUser)){
            message = String.format("User \"%s\" already exists", user);
        } else {
            newUser.setPassword(req.getParameter("pass"));
            users.getUsers().put(newUser, new ArrayList<String>());
            message = String.format("User \"%s\" created", user);
        }
        req.setAttribute("message", message);
        final RequestDispatcher rd = getServletContext().getRequestDispatcher("/register.jsp");
        rd.forward(req, resp);
    }
}
