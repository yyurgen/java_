/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gwain.authweb.servlets;

import com.gwain.authweb.entities.ChatUsers;
import java.beans.Beans;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Web application lifecycle listener.
 *
 * @author Конарх
 */
public class LifeListener implements ServletContextListener {
       
    public void contextInitialized(ServletContextEvent sce) {
//        final ServletContext ctx = sce.getServletContext();
//        try {
//            final File dataFile = new File(ctx.getRealPath(ctx.getInitParameter("data.file"))).getCanonicalFile();
//            if (dataFile.isFile()) {
//                ctx.log(String.format("------------------------------Loading ChatUsers from: %s", dataFile.getAbsolutePath()));
////                final ObjectInputStream in = new ObjectInputStream(new FileInputStream(dataFile));
////                try {
////                    final ChatUsers users = (ChatUsers) in.readObject();
////                    ctx.setAttribute("users", users);
////                } finally {
////                    in.close();
////                }
//                final ChatUsers users = (ChatUsers)Beans.instantiate(this.getClass().getClassLoader(), "com.gwain.authweb.entities.ChatUsers");
//                ctx.setAttribute("users", users);
//            } else {
//                ctx.log("-----------------------------No serialized data found");
//            }
//        } catch (Exception ex) {
//            ctx.log("-----------------------------Error during load", ex);
//        }
    }

    public void contextDestroyed(ServletContextEvent sce) {
        final ServletContext ctx = sce.getServletContext();
        final ChatUsers users = (ChatUsers) ctx.getAttribute("users");
        try {
            final File dataFile = new File(ctx.getRealPath(ctx.getInitParameter("data.file"))).getCanonicalFile();
            ctx.log(String.format("---------------------------Saving ChatUsers to: %s", dataFile.getAbsolutePath()));
            final ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(dataFile));
            try {
                out.writeObject(users);
            } finally {
                out.close();
            }
        } catch (Exception ex) {
            ctx.log("Error during save", ex);
        }
    }
}
