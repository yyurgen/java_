package com.gwain.authweb.servlets;

import com.gwain.authweb.entities.ChatUser;
import com.gwain.authweb.entities.ChatUsers;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author
 */
public class LoginUserServlet extends HttpServlet {

    private ChatUsers users;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user = req.getParameter("user");
        String pass = req.getParameter("pass");
        if (users==null){
            users = (ChatUsers) getServletContext().getAttribute("users");
        }
        for (Object obj : users.getUsers().keySet()){
            if ((((ChatUser)obj).getUser()).equals(user) && 
                (((ChatUser)obj).getPassword()).equals(pass)){
                req.getSession().setAttribute("curuser", obj);
                resp.sendRedirect("protect/msg.jsp");
                return;
            }
        }
        resp.sendError(HttpServletResponse.SC_FORBIDDEN);
    }

}