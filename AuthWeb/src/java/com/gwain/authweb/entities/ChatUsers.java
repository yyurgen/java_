/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gwain.authweb.entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 *
 * @author Конарх
 */
public class ChatUsers implements Serializable{
    private Map<ChatUser, List<String>> users = new HashMap<ChatUser, List<String>>();

    public Map<ChatUser, List<String>> getUsers() {
        return users;
    }
    
}
