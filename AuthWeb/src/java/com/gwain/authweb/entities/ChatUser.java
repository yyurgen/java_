package com.gwain.authweb.entities;

import java.io.Serializable;

/**
 *
 * @author Bobo
 */
public class ChatUser implements Serializable{
    private String user = null;
    private String password = null;

    public ChatUser() {
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User= " + user + " password= " + password;
    }

    @Override
    public int hashCode() {
        final int prime = 113;
        int hash = 1;
        hash = hash*prime + ((user==null) ? 0 : user.hashCode());
        //hash = hash*prime + (isLogged ? 3 : 5);//for boolean
        //hash = hash*prime + ((password==null) ? 0 : password.hashCode());
        return hash;
    }
    
    public boolean isRegisteredUser(){
        return (user != null) && (password != null);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass()!=ChatUser.class) {
            return false;
        }
        final ChatUser temp = (ChatUser) obj;
        if (temp.user==null){
            if (this.user!=null){
                return false;
            }
        } else {
            if (!temp.user.equals(this.user)){
                return false;
            }
        }
        return true;
    }
}
