<%@page import="com.gwain.authweb.entities.ChatUser" pageEncoding="utf-8"%>
<%@page import="java.util.*"%>
<jsp:useBean id="curuser" class="com.gwain.authweb.entities.ChatUser" scope="session"/>
<% String title = "Псевдочат";%>
<%@include file="../WEB-INF/jspf/header.jspf" %>
    <body><center>
        <h1>Псевдочат.</h1>
        <div class="form-title"><h3>User <%= curuser.getUser()%></h3></div>
        <form action="send" method="POST" class="form-container">
            <div class="form-title">Text</div>
            <input class="form-field" type="text" name="mess"  size="30"><br>
            <input class="submit-button" type="submit" value="OK!">
        </form><br>
        <div class="textout">
            <u>Output for user <strong><%= curuser.getUser()%></strong>:</u><br><br>
            <ul>
                <% for (String mess : users.getUsers().get(curuser)){ %>
                    <li><%= mess%></li>
                <%}%>
            </ul>
        </div>
        <a href="logout" class="submit-button">Logout</a>&nbsp;
    </center>
</body>
</html>
