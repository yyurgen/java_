<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% String title = "Логин";%>
<%@include file="WEB-INF/jspf/header.jspf" %>
    <body><center>
        <h3><%= session.getAttribute("message")!=null ? session.getAttribute("message") : ""%></h3>
        <%
            session.removeAttribute("message");
        %>
        <form action="login" method="POST" class="form-container">
            <div class="form-title"><h2>Login</h2></div>
            <div class="form-title">Username </div>
            <input class="form-field" type="text" name="user" value="" /><br>
            <div class="form-title">Password </div>
            <input class="form-field" type="password" name="pass" value="" /><br>
            <input class="submit-button" type="submit" value="Login" />
        </form>
        <hr>
        <a href="register.jsp" class="submit-button">Зарегистрируйтесь</a>
    </center>
    </body>
</html>
