/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gwain.restex;

import com.gwain.restex.entities.YandexSpellerResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

/**
 *
 * @author Конарх
 */
public class YandexSpellCheckerServlet extends HttpServlet {
    private DefaultHttpClient client = new DefaultHttpClient();
    private final BasicResponseHandler handler = new BasicResponseHandler();
    ObjectMapper mapper =new ObjectMapper();
    
    @Override
    public void init(){
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        final String word = req.getParameter("word");
        System.out.println(word);
        final HttpPost post = new HttpPost("http://speller.yandex.net/services/spellservice.json/checkText");
        post.setHeader("Content-Encoding", "UTF-8");//Not really needed here
        final List<NameValuePair> postContent = new ArrayList<NameValuePair>();
        postContent.add(new BasicNameValuePair("text", word));
        postContent.add(new BasicNameValuePair("lang", "ru"));
        post.setEntity(new UrlEncodedFormEntity(postContent, "utf-8"));
        try{
            final String json = handler.handleResponse(client.execute(post));
            //System.out.println(String.format("json: %s", json));
            YandexSpellerResponse[] variants = mapper.readValue(json, YandexSpellerResponse[].class);
            if (variants.length>0){
                req.setAttribute("variants", variants[0].getS());
            }
        } finally {
            post.releaseConnection();
        }
        final RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");
        rd.forward(req, resp);
    }
    
}
