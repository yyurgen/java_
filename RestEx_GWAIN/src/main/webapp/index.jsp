<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Spellchecker</title>
    </head>
    <body>
        <h3>Enter a word to check:</h3>
        <form action="check" method="post">
            <input size="20" name="word" value="<c:out value="${param['word']}"/>">
            <input type="submit" value="Проверить">
        </form>
        <ul>
        <c:forEach var="variant" items="${variants}">
            <li><c:out value="${variant}"/>
        </c:forEach>
        </ul>
    </body>
</html>
