/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package stat;

/**
 *
 * @author Конарх
 */
public class StaticEx {
    private int count;
    private static int statCount;
    
    static {
        statCount = 1;
    }
    
    public StaticEx(){
        count++;
        statCount++;
        System.out.printf("count=%d, static count=%d\n", count, statCount);
    }
    
    public static void main(String[] args){
        StaticEx.printStat();
        for (int i=0; i<10; i++){
            new StaticEx();
        }
    }
    
    public static void printStat(){
        System.out.printf("Initial static=%d\n", StaticEx.statCount);
        /*StaticEx sex = new StaticEx();
        sex.count=4;*/
    }
}
