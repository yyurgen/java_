<%-- 
    Document   : index
    Created on : Jun 6, 2013, 9:47:15 PM
    Author     : Конарх
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page from Spring</title>
    </head>
    <body>
        <h1>Hello World From Spring!</h1>
        <c:out value="${searchable}"/>
        <a href="message.html">message</a>
    </body>
</html>
