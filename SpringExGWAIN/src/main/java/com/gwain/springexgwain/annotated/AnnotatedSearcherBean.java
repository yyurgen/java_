/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gwain.springexgwain.annotated;

import com.gwain.springexgwain.beans.SearchableBean;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author Конарх
 */
@Component
public class AnnotatedSearcherBean{
    @Autowired
    @Qualifier("s2")
    private SearchableBean searchable;
//    private List<SearchableBean> searchables;
    
    
    public SearchableBean getSearchable(){
//        return searchables.get(0);
        return searchable;
    }
}
