/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gwain.springexgwain.beans;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 * @author Конарх
 */
public class SearcherBean implements ApplicationContextAware{
    private ApplicationContext ctx;
    
    @Override
    public void setApplicationContext(ApplicationContext ac) throws BeansException {
        ctx = ac;
    }
    
    public SearchableBean getSearchable(){
        //Type binding
        //return (SearchableBean)ctx.getBean(SearchableBean.class);
        //Quilifier buinding
        //return (SearchableBean)ctx.getBean("s1");
        //Collection binding
        return ctx.getBeansOfType(SearchableBean.class).values().iterator().next();
    }
    
}
