/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gwain.springexgwain.controllers;

import com.gwain.springexgwain.beans.SearcherBean;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.AbstractCommandController;
import org.springframework.web.servlet.mvc.AbstractController;

/**
 *
 * @author Конарх
 */
public class IndexController extends AbstractController{
    private SearcherBean searcher;
    
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        System.out.println(String.format("Index controller called"));
        final ModelAndView reply = new ModelAndView();
        reply.setViewName("index");
        reply.addObject("searchable", searcher.getSearchable());
        return reply;
    }

    public void setSearcher(SearcherBean searcher) {
        this.searcher = searcher;
    }

}
