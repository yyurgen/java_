/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gwain.springexgwain.controllers;

import com.gwain.springexgwain.entites.MessageBean;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

/**
 *
 * @author Конарх
 */
public class FormMessageController extends SimpleFormController{
    
    public FormMessageController(){
        this.setFormView("message");
        this.setCommandClass(MessageBean.class);
    }

    @Override
    protected ModelAndView onSubmit(Object command) throws Exception {
        final ModelAndView mv = new ModelAndView("message");
        mv.addObject("message", ((MessageBean)command).getMessage());
        return mv;
    }
    
}
