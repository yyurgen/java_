/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reflection.cl_isolation;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.SecureClassLoader;

/**
 *
 * @author Конарх
 */
public class ClassLoaderIsolationMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InstantiationException, ClassNotFoundException, IllegalAccessException, MalformedURLException {
        /*ClassWithStatic a = new ClassWithStatic();
        ClassWithStatic b = new ClassWithStatic();*/
        ClassWithStaticInt a = (ClassWithStaticInt) Class.forName("reflection.cl_isolation.ClassWithStatic", true, new URLClassLoader(new URL[]{new File("tmp").toURI().toURL()})).newInstance();
        ClassWithStaticInt b = (ClassWithStaticInt) Class.forName("reflection.cl_isolation.ClassWithStatic", true, new URLClassLoader(new URL[]{new File("tmp").toURI().toURL()})).newInstance();
        a.setA("Value A");
        b.setA("Value B");
        System.out.println(a.getA());
        System.out.println(b.getA());
    }
}
