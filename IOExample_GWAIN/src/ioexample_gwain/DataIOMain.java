/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioexample_gwain;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

/**
 *
 * @author Конарх
 */
public class DataIOMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final String filename = "somefile.txt";
        final Random rnd = new Random();
        try {
            final DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));
            try{
                for (int i=0; i<10; i++){
//                    final long l = rnd.nextLong();
//                    out.writeLong(l);
//---------------------------------------------------
//                    final int l = rnd.nextInt(255);
//                    out.write(l);
//                    System.out.printf("%d ", l);
//---------------------------------------------------                    
                    if (rnd.nextBoolean()){
                        final int l = Math.abs(rnd.nextInt());
                        out.writeInt(l);
                        System.out.printf("%d ", l);
                    } else {
                        final long l = -Math.abs(rnd.nextLong());
                        out.writeLong(l);
                        System.out.printf("%d ", l);
                    }
                }
                System.out.println();
            } finally {
                out.close();
            }
            final BufferedInputStream buf = new BufferedInputStream(new FileInputStream(filename));
            final DataInputStream in = new DataInputStream(buf);
            try{
                
//                while (in.available()>0){
//                    final long val = in.readLong();
//-------------------------------------------------
//                byte[] data = new byte[10];
//                final int size = in.read(data);
                    //final int val = (int)data[i];
//                for (int i=0; i<size; i++){
//                    final int val = data[i]&0xff;
//                    System.out.printf("%d ", val);
//                }
                while (in.available()>0){
                    long val = 0;
                    if (in.available()>4){
                        buf.mark(8);
                        val = in.readLong();
                        if (val>0){
                            buf.reset();
                            val = in.readInt();
                        }
                    } else {
                        val = in.readInt();
                    }
                    System.out.printf("%d ", val);
                }
                System.out.println();
            } finally {
                in.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
