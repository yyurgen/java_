/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ioexample_gwain;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Конарх
 */
public class IOExampleMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final String filename = "somefile.txt";
        try {
            final OutputStream out = new FileOutputStream(filename);
            try{
                for (int i=0; i<10; i++){
                    out.write(String.format("%d ", i).getBytes());
                }
            } finally {
                out.close();
            }
            final InputStream in = new FileInputStream(filename);
            try{
                while (in.available()>0){
                    final int val = in.read();
                    if (' '!=val){
                        final int num = val-'0';
                        System.out.println(num);
                    }
                }
            } finally {
                in.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }
}
