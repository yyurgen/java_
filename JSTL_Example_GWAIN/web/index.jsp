<%-- 
    Document   : index
    Created on : Apr 19, 2013, 9:33:05 PM
    Author     : Конарх
--%>

<%@page import="com.gwain.jstlex.Message"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="messages" class="com.gwain.jstlex.Messages" scope="application"/>
<!DOCTYPE html>
<c:if test="${param.add != null}">
    <jsp:useBean id="newMess" class="com.gwain.jstlex.Message" scope="page">
        <jsp:setProperty name="newMess" param="mess" property="message"/>
        <c:choose>
            <c:when test="${param['isLeft'] eq 'on'}">
                <c:set target="${newMess}" property="isLeft" value="true"/>
            </c:when>
            <c:otherwise>
                <c:set target="${newMess}" property="isLeft" value="false"/>
            </c:otherwise>
        </c:choose>
        <c:set target="${messages}" property="newMessage" value="${newMess}"/>
    </jsp:useBean>
</c:if>
<%-- if (request.getParameter("add")!=null){
    String mess = request.getParameter("mess");
    String isLeft = request.getParameter("isLeft");
    final Message message = new Message();
    message.setMessage(mess);
    message.setIsLeft(isLeft!=null);
    messages.getMessages().add(message);
}--%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sample JSTL page</title>
    </head>
    <body>
        <table border="1">
            <c:set var="from" scope="page" value="0"/>
            <c:if test="${param.start != null}">
                <c:set var="from" scope="page" value="${param.start}"/>
            </c:if>
            <c:forEach var="currMess" items="${messages.messages}" varStatus="messStat">
                <c:if test="${(messStat.index>=from) && (messStat.index<(from+4))}">
                    <tr>
                        <td>
                            <c:if test="${currMess.isLeft}">
                                <c:out value="${currMess.message}"/>
                            </c:if>
                        </td>
                        <td>
                            <c:if test="${currMess.isLeft == false}">
                                <c:out value="${currMess.message}"/>
                            </c:if>
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
                <%--<%for (Message m : messages.getMessages()){%>
                    <tr>
                
                    <%if (m.isIsLeft()){%>
                    <td><%= m.getMessage()%></td>
                    <td></td>
                    <%} else {%>
                    <td></td>
                    <td><%= m.getMessage()%></td>
                    <%}%>
                    
                    </tr>
                 <%}%>--%>
        </table>
        <!-- Pager -->
        <c:choose>
            <c:when test="${from>0}">
                <a href="index.jsp?start=<c:out value="${from-4}"/>"><<</a>
            </c:when>
            <c:otherwise>
                <<
            </c:otherwise>
        </c:choose>
        <c:choose>
            <c:when test="${from+4<fn:length(messages.messages)}">
                <a href="index.jsp?start=<c:out value="${from+4}"/>">>></a>
            </c:when>
            <c:otherwise>
                >>
            </c:otherwise>
        </c:choose>
        <p>
            <form method="GET" action="#">
                <input type="text" name="mess" size="20">
                <input type="checkbox" name="isLeft" value="on"><br>
                <input type="submit" name="add" value="Add data"/>
            </form>
        </p>
    </body>
</html>
