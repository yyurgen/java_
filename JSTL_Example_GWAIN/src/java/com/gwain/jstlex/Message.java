package com.gwain.jstlex;

/**
 *
 * @author Конарх
 */
public class Message {
    private String message;
    private boolean isLeft;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isIsLeft() {
        return isLeft;
    }

    public void setIsLeft(boolean isLeft) {
        this.isLeft = isLeft;
    }
   
}
