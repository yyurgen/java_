<%@page import="java.io.IOException" errorPage="../error.jsp"%>
<%@page import="com.sugar.galery.RandomGaleryImagesFetcher"%>
<% String title = "Change Sugar Here - main page"; %>
<%@include  file="../WEB-INF/jspf/header.jspf"%>
        <script type="text/javascript">
            $(document).ready ( function() {
                $("#loginText").css("border", "1px solid #999");
                $("#passwordText").css("border", "1px solid #999");
                
                $("#clearButton").on('click', function (e) {
                    $("#loginText").css("border", "1px solid #999");
                    $("#passwordText").css("border", "1px solid #999");
                });
                
                $("#submitButton").on('click', function (e) {
                    var loginLength = $("#loginText").val().length;
                    var passwordLength = $("#passwordText").val().length;
                    
                    if (loginLength === 0 && passwordLength === 0) {
                        $("#loginText").css("border", "1px solid red");
                        $("#passwordText").css("border", "1px solid red");
                        
                        return false;
                    }
                    
                    if (loginLength !== 0 && passwordLength === 0) {
                        $("#loginText").css("border", "1px solid #999");
                        $("#passwordText").css("border", "1px solid red");
                        
                        return false;
                    }
                    
                    if (loginLength === 0 && passwordLength !== 0) {
                        $("#loginText").css("border", "1px solid red");
                        $("#passwordText").css("border", "1px solid #999");
                        
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>
        <% if (true) throw new IOException("Sample IO"); %>
        <center>
            <div id='caption'>
                Change<font color='#C0C0C0'> Sugars</font> Here
            </div>
            <div class='littlegreytext'>
                <br />
                sugar exchange is so simple...
            </div>
            
            <div id='container'>
                <div style='height : 20px'>&nbsp;</div>
                <div id='login' class='littlegreytext'>
<% 
                    if (true == user.isNewUser()) {
%>
                        <form action='../LoginServlet' method='post'>
                            Login <br />
                            <input id='loginText' style='margin-top : 5px; margin-bottom : 10px;' maxlength='10' type='text' name='login' value=''/><br />
                            Password <br />
                            <input id='passwordText' style='margin-top : 5px; margin-bottom : 10px;' maxlength='10' type='password' name='password' value=''/><br />
                            <input id='submitButton' style='margin-top : 5px; margin-bottom : 5px;' type='submit' value='Login'>
                            <input id='clearButton' type="reset" value="Clear">
                            <br />
                            <a class='littlegreylink' href='registration.jsp'>Register now</a>
                        </form>
<%
                    } else {
                        if (true == user.isAdmin()) {
%>                       
                            <a class='littlegreylink' href='adminPanel.jsp'>Admin panel</a>
<%                          
                        }
%>
                        <h3>Welcome!<br /><br /><%= user.getLogin()%></h3>
                        <form action="../LogoutServlet" method="post">
                            <input type="submit" value="Logout">
                        </form>
<%                      
                    }
%>
                </div>
                <div style='float: left; width : 10px;'>&nbsp;</div>
                <div id='randomgalery'>
<% 
                    for (int i=0; i<5; i++) {
                        String imagePath = RandomGaleryImagesFetcher.fetchRandomImageName();                        
%>
                        <img src="../<%= imagePath%>" style="width : 140px; height : 140px" />
<%
                    }
%>
                    <div>
                        <img style='margin-top : 10px; margin-bottom : 10px; width : 20px' src='../images/refresh.png'>
                    </div>
                </div>
            </div>
        </center>
    </body>
</html>