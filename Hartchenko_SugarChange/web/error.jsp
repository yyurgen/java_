<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true"%>
<% application.log("Error page called"); %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error page</title>
    </head>
    <body>
        <center>
            <div style='width : 100%; height : 300px; background-color : red; border : 1px solid black; color: white'>
                <table style='height : 300px; width : 100%'>
                    <tr>
                        <td style='font-size : 40px; height : 300px; text-align : center; vertical-align : middle'>
                            <%=exception %>
                        </td>
                    </tr>
                </table>
            </div>
        </center>
    </body>
</html>