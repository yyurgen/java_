package com.sugar.servlets;

import com.sugar.users.SystemUsers;
import com.sugar.users.User;
import com.sugar.users.UserImpl;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.captcha.Captcha;

/**
 *
 * @author LordNighton
 * 
 */

@WebServlet(name = "RegistrationServlet", urlPatterns = {"/RegistrationServlet"})
public class RegistrationServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        if (true == isCaptchaCorrect(request)) {
            final String login = (String) request.getParameter("login");
            final String password = (String) request.getParameter("password");

            final SystemUsers sysUsers = (SystemUsers) request.getServletContext().getAttribute("sysUsers");
            User user = (User) request.getSession().getAttribute ("user");
            
            final UserImpl adminUser = (UserImpl) request.getServletContext().getAttribute ("adminUser");
            if (true == adminUser.getLogin().equals(login) && true == adminUser.getPassword().equals(password)) {
                user.setLogin (login);
                user.setPassword (password);
                user.setIsAdmin (true);

                response.sendRedirect ("protected/index.jsp");
            } else { 
                final User sysUser = sysUsers.getUserByLogin (login);
                if (null == sysUser) {
                    UserImpl newUser = new UserImpl();
                    newUser.setLogin (login);
                    newUser.setPassword (password);
                    newUser.setIsAdmin (false);

                    request.getSession().setAttribute("user", newUser);

                    sysUsers.addUser(newUser);
                } else {
                    if (true == sysUser.getLogin().equals(login) && true == sysUser.getPassword().equals(password)) {
                        if (true == user.isNewUser()) {
                            user.setLogin (login);
                            user.setPassword (password);
                            user.setIsAdmin (false);
                        }
                    }
                }

                response.sendRedirect("protected/index.jsp");
            }
        } else {
            response.sendRedirect("protected/registration.jsp");
        }
    }
    
    private boolean isCaptchaCorrect (HttpServletRequest request) {
        Captcha captcha = (Captcha) request.getSession().getAttribute (Captcha.NAME);
        String answer = request.getParameter("answer");
        if (captcha.isCorrect(answer)) {
            return true;
        } else { 
            return false;
        } 
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    @Override
    public String getServletInfo() {
        return "Registration Servlet";
    }
    
}