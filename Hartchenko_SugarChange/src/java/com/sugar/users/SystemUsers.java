package com.sugar.users;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author LordNighton
 *
 */

public class SystemUsers implements Serializable {

    private List <User> users = new ArrayList <User> ();
    
    public void setUsers (List <User> users) {
        this.users = users;
    }
    
    public List <User> getUsers () {
        return this.users;
    }
    
    public void addUser (User newUser) {
        this.users.add(newUser);
    }
    
    public User getUserByLogin (String login) {
        for (User next : users) {
            if (true == login.equals (next.getLogin())) {
                return next;
            }
        }
        
        return null;
    } 
    
    @Override
    public String toString () {
        String result = "";
        for (User next : users) {
            result += "user -> [" + next.getLogin() + "]<br />";
        }
        return result;
    }
            
}