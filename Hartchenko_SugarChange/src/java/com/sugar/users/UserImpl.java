package com.sugar.users;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * @author LordNighton
 * 
 */

public class UserImpl implements User, Serializable {

    private String login = "";
    
    private String password = "";
    
    private boolean isAdmin = false;
    
    private ArrayList <String> messages = new ArrayList <String> ();

    @Override
    public String getLogin() {
        return this.login;
    }
    
    @Override
    public void setLogin (String login) {
        this.login = login;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public void setPassword (String password) {
        this.password = password;
    }
    
    public boolean isNewUser () {
        if (false == getLogin().isEmpty() && false == getPassword().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isAdmin() {
        return this.getIsAdmin();
    }

    @Override
    public void setIsAdmin (boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public boolean getIsAdmin() {
        return this.isAdmin;
    }
    
}