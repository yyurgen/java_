package com.sugar.users;

import java.util.ArrayList;

/*
 *
 * @author LordNighton
 * 
 */

public interface User {

    public String getLogin ();
    
    public void setLogin (String login);
    
    public String getPassword ();
    
    public void setPassword (String password); 
   
    public void setIsAdmin (boolean isAdmin);
    
    public boolean getIsAdmin ();
    
    public boolean isNewUser();
    
    public boolean isAdmin();
    
}