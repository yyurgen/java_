package com.sugar.listeners;

import com.sugar.users.SystemUsers;
import com.sugar.users.UserImpl;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 *
 * @author LordNighton
 * 
 */

@WebListener()
public class LifeListener implements ServletContextListener {

    public static final String ADMINUSER_LOGIN_PARAM = "adminUserLogin";
    
    public static final String ADMINUSER_PASSWORD_PARAM = "adminUserPassword";
    
    public static final String DATA_FILE = "data.file";
    
    @Override
    public void contextInitialized (ServletContextEvent sce) {
        final ServletContext context = sce.getServletContext();
        
        fillSuperUserBean (context);
        
        SystemUsers sysUsers = null;
        try {
            final File data = retrieveDataFile(context);

            if (true == data.isFile()) {
                ObjectInputStream in = new ObjectInputStream (new FileInputStream (data));
                try {
                    sysUsers = (SystemUsers) in.readObject();
                    context.setAttribute("sysUsers", sysUsers);
                } finally {
                    in.close();
                }
            }
        } catch (Exception e) {
            context.log ("IOException while reading sysUsers FROM file", e);
        }
    }

    @Override
    public void contextDestroyed (ServletContextEvent sce) {
        final ServletContext context = sce.getServletContext();
        final SystemUsers sysUsers = (SystemUsers) context.getAttribute ("sysUsers");
        
        if (null != sysUsers) {
            try {
                final File data = retrieveDataFile(context);
                
                ObjectOutputStream out = new ObjectOutputStream (new FileOutputStream (data));
                try {
                    out.writeObject (sysUsers);
                } finally {
                    out.close();
                } 
            } catch (IOException e) {
                 context.log ("IOException while saving sysUsers TO file", e);
            }
        } 
    }
    
    private File retrieveDataFile (ServletContext context) throws IOException {
        String dataFilePath = context.getInitParameter (DATA_FILE);
        String realFilePath = context.getRealPath (dataFilePath);
        File dataFile = new File (realFilePath).getCanonicalFile();
        
        return dataFile;
    }

    private void fillSuperUserBean (ServletContext context) {
        UserImpl superUser = new UserImpl ();
        
        superUser.setLogin (context.getInitParameter (ADMINUSER_LOGIN_PARAM));
        superUser.setPassword (context.getInitParameter (ADMINUSER_PASSWORD_PARAM));
        
        context.setAttribute ("adminUser", superUser);
    }
    
}