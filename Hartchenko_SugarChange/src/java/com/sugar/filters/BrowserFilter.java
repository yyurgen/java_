package com.sugar.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author LordNighton
 * 
 */

@WebFilter(filterName = "BrowserFilter", urlPatterns = {"/protected/*"})
public class BrowserFilter implements Filter {
    
    public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse resp = (HttpServletResponse) response;
        
        if (true) {
            //throw new IllegalArgumentException("You are using an incorrect version of browser. IE is prohibited.");
        }
        
        if (req.getHeader("User-Agent").contains("MSIE")) {
//            req.getSession().setAttribute("errorText", "You are using an incorrect version of browser. IE is prohibited.");
//            resp.sendRedirect("../error.jsp");
//            throw new IllegalArgumentException("You are using an incorrect version of browser. IE is prohibited.");
        } else {
            chain.doFilter (request, response);
        }
    }

    public void destroy() {        
    }

    @Override
    public void init (FilterConfig filterConfig) throws ServletException {
    }

}