/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gwain.simpleweb;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Конарх
 */
public class HelloServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //resp.sendError(HttpServletResponse.SC_FORBIDDEN);
        //resp.sendRedirect("index.html");
        resp.setContentType("text/plain");
        final String someText = "Some text Какой-то текст 一部のテキスト";
        //final PrintStream out = new PrintStream(resp.getOutputStream());
        //resp.setBufferSize(10);
        resp.setCharacterEncoding("utf-8");
        final PrintWriter w = resp.getWriter();
        //out.println(someText);
        w.println(someText);
        //resp.setContentType("image/jpeg");
    }
    
}
