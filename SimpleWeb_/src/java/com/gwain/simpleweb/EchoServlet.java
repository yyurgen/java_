/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gwain.simpleweb;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Конарх
 */
public class EchoServlet extends HttpServlet {
    //private String mess = null;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        final PrintWriter w = resp.getWriter();
        w.println("<html><head>");
        w.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
        w.println("</head><body>");
        w.println("<h2>");
        final String mess = (String) req.getAttribute("mess");
        if ((mess!=null) && (!mess.trim().isEmpty())){
            w.println(mess);
        } else {
            w.println("Enter some in url: ?mess=...");
        }
        w.println("</h2>");
        w.println("<form action=\"echo\" method=\"post\">");
        w.println("<input type=\"text\" name=\"mess\" size=30>");
        w.println("<input type=\"submit\" value=\"Отправить\">");
        w.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        String mess = req.getParameter("mess");
        req.setAttribute("mess", mess);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        //resp.sendRedirect("echo");
        doGet(req, resp);
    }
   
}
